
import Vue from 'vue';
import VueRouter  from 'vue-router'
import Home from '@/components/Home'
import Policy from '@/components/Policy'
import Terms from '@/components/Terms'
import NotFound from '@/components/NotFound'

Vue.use(VueRouter )


const router = new VueRouter ({
    mode: "history",
    routes: [
        { path: "/",
            component: Home },
        { path: "/policy", name: "Policy", component: Policy },
        { path: "/terms", name: "Terms", component: Terms },
        { path: "*", name: "NotFound", component: NotFound },
    ],
})

export default router