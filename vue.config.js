module.exports = {
    pages: {
        index: {
            entry: 'src/main.js',
            template: 'public/index.html',
            filename: 'index.html'
        },
        policy: {
            entry: 'src/main.js',
            template: 'public/index.html',
            filename: 'policy.html'
        },
        terms: {
            entry: 'src/main.js',
            template: 'public/index.html',
            filename: 'terms.html'
        },        
    }
}